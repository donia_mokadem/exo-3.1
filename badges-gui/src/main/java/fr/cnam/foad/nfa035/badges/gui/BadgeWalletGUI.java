package fr.cnam.foad.nfa035.badges.gui;

import fr.cnam.foad.nfa035.badges.wallet.dao.DirectAccessBadgeWalletDAO;
import fr.cnam.foad.nfa035.badges.wallet.dao.impl.DirectAccessBadgeWalletDAOImpl;
import fr.cnam.foad.nfa035.badges.wallet.model.DigitalBadge;

import javax.swing.*;
import javax.swing.table.TableModel;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.security.Provider;
import java.text.CharacterIterator;
import java.text.StringCharacterIterator;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

public class BadgeWalletGUI {
    private static final String RESOURCES_PATH = "badges-gui/src/main/resources/";


    private JButton button_msg;
    private JPanel panelMain;
    private JTable table1;

    public BadgeWalletGUI() {
        button_msg.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                JOptionPane.showMessageDialog(null,"Bien joué!");
            }
        });
        DirectAccessBadgeWalletDAO dao = null;

        try {


            dao = new DirectAccessBadgeWalletDAOImpl(RESOURCES_PATH + "db_wallet_indexed.csv");
            Set<DigitalBadge> metaSet = dao.getWalletMetadata();

           List<DigitalBadge> tableList= new ArrayList<>();

            tableList.addAll(metaSet);
            TableModel tableModel = TableModelCreator.createTableModel(DigitalBadge.class,  tableList);
            table1.setModel(tableModel);

        } catch (IOException ioException) {
            ioException.printStackTrace();
        }
    }



    public static void main(String[] args) {

        JFrame frame  = new JFrame("BadgeWalletGUI");

        frame.setContentPane(new BadgeWalletGUI().panelMain);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.pack();
        frame.setVisible(true);
    }



    private String humanReadableByteCountBin(long bytes) {
        long absB = bytes == Long.MIN_VALUE ? Long.MAX_VALUE : Math.abs(bytes);
        if (absB < 1024) {
            return bytes + " o";
        }
        long value = absB;
        CharacterIterator ci = new StringCharacterIterator("KMGTPE");
        for (int i = 40; i >= 0 && absB > 0xfffccccccccccccL >> i; i -= 10) {
            value >>= 10;
            ci.next();
        }
        value *= Long.signum(bytes);
        return String.format("%.1f %co", value / 1024.0, ci.current());
    }

}
